# Weather App

## Installation

**Clone the repo locally:**

```sh
git clone https://gitlab.com/KookWooi/weather-app.git
cd weather-app
```

**Create .env file and paste the contents of the .env.example file into it.**

**Install PHP dependencies:**

```sh
composer install
```

**Install NPM dependencies:**

```sh
npm i
```

**Build assets:**

```sh
npm run dev
```

**Generate application key:**

```sh
php artisan key:generate
```

**Run the dev server (the output will give the address):**

```sh
php artisan serve
```

## How to use

**To view weather data, you must first type in the city name and then select an interval.** 

![UI](./public/tutorial/interface1.jpg)

**The weather forecast will be displayed as follows:** 

![UI](./public/tutorial/interface2.jpg)

**You can delete weather data by hovering over it and using the delete button.** 

![UI](./public/tutorial/interface3.jpg)