<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class WeatherController extends Controller{

    public function getWeatherInfo(Request $request){
        $apikey = config('services.openweather.key');
        $city = $request['city'];
        
        $dir_geo_info = $this->get_geo_info($city, $apikey);

        if(count($dir_geo_info) > 0){
            $lat = $dir_geo_info[0]['lat'];
            $lon = $dir_geo_info[0]['lon'];

            $open_weather_info = $this->get_open_weather_info($lat, $lon, $apikey);
            
            return $open_weather_info;
        }else{
            return["errorMessage"=>'No city found.'];
        }
    }

    public function get_geo_info($city, $apikey){
        try {
            $client = new Client();
            $geo_url = "http://api.openweathermap.org/geo/1.0/direct?q={$city}&limit=5&appid={$apikey}";

            $response = $client->get($geo_url);

            $dir_geo_info = json_decode($response->getBody(), true);

            return $dir_geo_info;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function get_open_weather_info($lat, $lon, $apikey){
        try {
            $client = new Client();
            $open_weather_url = "http://api.openweathermap.org/data/2.5/weather?lat={$lat}&lon={$lon}&appid={$apikey}&units=metric";

            $response = $client->get($open_weather_url);

            return json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $e) {
            return null;
        }
    }
}